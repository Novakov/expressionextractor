﻿using ICSharpCode.NRefactory.CSharp;
using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor
{
    static class AstExtensions
    {
        public static bool IsGetterCall(this MemberReferenceExpression @this)
        {
            var property = @this.Annotation<PropertyReference>();

            var method = @this.Annotation<MethodReference>();

            return (property != null)
                && (method != null)
                && (property.Resolve().GetMethod == method.Resolve());
        }

        public static bool IsFieldReference(this MemberReferenceExpression @this)
        {
            var field = @this.Annotation<FieldReference>();

            return field != null;
        }

        public static bool IsEnumReference(this MemberReferenceExpression @this)
        {
            var field = @this.Annotation<FieldReference>();

            return field != null
                && field.DeclaringType.Resolve().IsEnum;
        }
    }
}
