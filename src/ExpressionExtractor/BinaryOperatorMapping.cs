﻿using ICSharpCode.NRefactory.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor
{
    static class OperatorMapping
    {
        private static readonly Dictionary<BinaryOperatorType, MethodInfo> binaryMappings = new Dictionary<BinaryOperatorType, MethodInfo>
        {
            {BinaryOperatorType.BitwiseAnd ,ExpressionType.And},
            {BinaryOperatorType.BitwiseOr ,ExpressionType.Or},
            {BinaryOperatorType.ConditionalAnd ,ExpressionType.And},
            {BinaryOperatorType.ConditionalOr ,ExpressionType.Or},
            {BinaryOperatorType.ExclusiveOr ,ExpressionType.ExclusiveOr},
            {BinaryOperatorType.GreaterThan ,ExpressionType.GreaterThan},
            {BinaryOperatorType.GreaterThanOrEqual ,ExpressionType.GreaterThanOrEqual },
            {BinaryOperatorType.Equality ,ExpressionType.Equal},
            {BinaryOperatorType.InEquality ,ExpressionType.NotEqual},
            {BinaryOperatorType.LessThan ,ExpressionType.LessThan},
            {BinaryOperatorType.LessThanOrEqual ,ExpressionType.LessThanOrEqual},
            {BinaryOperatorType.Add ,ExpressionType.Add},
            {BinaryOperatorType.Subtract ,ExpressionType.Subtract },
            {BinaryOperatorType.Multiply ,ExpressionType.Multiply },
            {BinaryOperatorType.Divide ,ExpressionType.Divide },
            {BinaryOperatorType.Modulus ,ExpressionType.Modulo},
            {BinaryOperatorType.ShiftLeft ,ExpressionType.LeftShift},
            {BinaryOperatorType.ShiftRight ,ExpressionType.RightShift},
            {BinaryOperatorType.NullCoalescing ,ExpressionType.Coalesce},
        };

        public static MethodInfo For(BinaryOperatorType type)
        {
            return binaryMappings[type];
        }
    }
}
