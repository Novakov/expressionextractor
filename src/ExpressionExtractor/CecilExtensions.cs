﻿using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor
{
    static class CecilExtensions
    {
        public static TypeReference DecomposeArrayType(this TypeReference @this, int dimensions)
        {
            var t = @this;
            for (int i = 0; i < dimensions; i++)
            {
                t = t.GetElementType();
            }

            return t;
        }
    }
}
