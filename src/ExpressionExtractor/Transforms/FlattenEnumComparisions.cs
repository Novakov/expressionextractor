﻿using ICSharpCode.Decompiler.Ast.Transforms;
using ICSharpCode.NRefactory.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mono.Cecil.Rocks;
using Mono.Cecil;
using ICSharpCode.Decompiler.Ast;

namespace ExpressionExtractor.Transforms
{
    class FlattenEnumComparisions : DepthFirstAstVisitor, IAstTransform
    {
        public override void VisitBinaryOperatorExpression(BinaryOperatorExpression binaryOperatorExpression)
        {
            base.VisitBinaryOperatorExpression(binaryOperatorExpression);

            var leftType = binaryOperatorExpression.Left.Annotation<ICSharpCode.Decompiler.Ast.TypeInformation>().InferredType.Resolve();
            if (leftType.IsEnum)
            {
                var left = binaryOperatorExpression.Left;
                left.Remove();
                binaryOperatorExpression.Left = left.CastTo(new PrimitiveType("int")); //TODO: use proper type
                binaryOperatorExpression.Left.AddAnnotation(new TypeInformation(leftType.GetEnumUnderlyingType()));
            }
        }

        public override void VisitMemberReferenceExpression(MemberReferenceExpression memberReferenceExpression)
        {
            if (memberReferenceExpression.IsEnumReference())
            {
                //TODO: Check if field is enum member
                var value = memberReferenceExpression.Annotation<FieldReference>().Resolve().Constant;

                var type = memberReferenceExpression.Annotation<FieldReference>().DeclaringType.Resolve().GetEnumUnderlyingType();

                var constant = new PrimitiveExpression(value);
                constant.AddAnnotation(type);
                constant.AddAnnotation(new TypeInformation(type));

                memberReferenceExpression.ReplaceWith(constant);
            }
            else
            {
                base.VisitMemberReferenceExpression(memberReferenceExpression);
            }
        }

        public void Run(AstNode compilationUnit)
        {
            compilationUnit.AcceptVisitor(this);
        }
    }
}
