﻿using ICSharpCode.Decompiler.Ast;
using ICSharpCode.Decompiler.Ast.Transforms;
using ICSharpCode.NRefactory.CSharp;
using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor.Transforms
{
    public class AppendTypeInformationToReturnStatement : DepthFirstAstVisitor, IAstTransform
    {
        private MethodDefinition method;

        public AppendTypeInformationToReturnStatement(MethodDefinition method)
        {
            this.method = method;
        }

        public override void VisitReturnStatement(ReturnStatement returnStatement)
        {
            returnStatement.RemoveAnnotations<TypeInformation>();
            returnStatement.AddAnnotation(new TypeInformation(method.ReturnType));
            base.VisitReturnStatement(returnStatement);
        }

        public void Run(AstNode compilationUnit)
        {
            compilationUnit.AcceptVisitor(this);
        }
    }
}
