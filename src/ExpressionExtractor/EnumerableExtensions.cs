﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor
{
    static class EnumerableExtensions
    {
        public static IEnumerable<Indexed<T>> AsIndexed<T>(this IEnumerable<T> @this)
        {
            return @this.Select((x, i) => new Indexed<T>(x, i));
        }

        public class Indexed<T>
        {
            public Indexed(T value, int index)
            {
                this.Value = value;
                this.Index = index;
            }

            public int Index { get; private set; }

            public T Value { get; private set; }
        }
    }
}
