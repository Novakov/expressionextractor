﻿using ExpressionExtractor.Builders;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using ICSharpCode.Decompiler;
using ICSharpCode.Decompiler.Disassembler;
using ICSharpCode.Decompiler.Ast;
using ICSharpCode.NRefactory;
using ICSharpCode.NRefactory.CSharp;

namespace ExpressionExtractor
{
    class InitializerBuilder
    {
        private MethodDefinition inputMethod;
        private FieldDefinition field;

        private ModuleDefinition module;

        public List<VariableDefinition> Variables { get; private set; }

        public InitializerBuilder(MethodDefinition inputMethod, FieldDefinition field)
        {
            this.inputMethod = inputMethod;
            this.field = field;

            this.module = inputMethod.Module;

            this.Variables = new List<VariableDefinition>();
        }

        public IEnumerable<Instruction> Build()
        {
            var context = new DecompilerContext(this.module);

            context.CurrentModule = this.module;
            context.CurrentType = this.inputMethod.DeclaringType;

            var astBuilder = new AstBuilder(context);

            astBuilder.AddMethod(this.inputMethod);

            astBuilder.RunTransformations();

            var returnStatment = astBuilder.SyntaxTree.Descendants.OfType<ReturnStatement>().SingleOrDefault();

            new Transforms.FlattenEnumComparisions().Run(returnStatment);
            new Transforms.AppendTypeInformationToReturnStatement(this.inputMethod).Run(returnStatment);

            var visitor = new BuilderVisitor(this.inputMethod, this.field);

            returnStatment.AcceptVisitor(visitor);

            this.Variables.AddRange(visitor.Variables);

            return visitor.Instructions;
        }
    }
}
