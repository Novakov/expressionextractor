﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor
{
    static class ReflectionHelper
    {
        public static MethodInfo GetMethod<TSelf>(Expression<Action<TSelf>> method)
        {
            var call = (MethodCallExpression)method.Body;

            return call.Method;
        }

        public static MethodInfo GetMethod(Expression<Action> method)
        {
            var call = (MethodCallExpression)method.Body;

            return call.Method;
        }
    }
}
