﻿using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor.Builders
{
    public class ExpressionBuilder
    {
        private readonly ILBuilder il;

        public ExpressionBuilder(ILBuilder il)
        {
            this.il = il;
        }

        public void Constant(TypeReference type, object value)
        {
            il.Push(value);
            if (type.IsValueType)
            {
                il.Box(type);
            }

            il.PushType(type);

            il.Call(ExpressionType.Constant);
        }

        public void Parameter(TypeReference type, string name)
        {
            il.PushType(type);
            il.Push(name);
            il.Call(ExpressionType.Parameter);
        }

        public void Property(MethodReference getter)
        {
            il.PushMethod(getter);
            il.CastClass(typeof(MethodInfo));
            il.Call(ExpressionType.Property);
        }

        public void Negate()
        {
            il.Call(ExpressionType.Negate);
        }

        public void Not()
        {
            il.Call(ExpressionType.Not);
        }

        public void Convert(TypeReference type)
        {
            il.PushType(type);
            il.Call(ExpressionType.Convert);
        }

        public void Call()
        {
            il.Call(ExpressionType.Call);
        }

        public void Field(FieldReference field)
        {
            il.PushFieldInfo(field);
            il.Call(ExpressionType.Field);
        }

        public void New(MethodReference ctor, TypeReference type)
        {
            il.PushMethod(ctor);
            il.CastClass(typeof(ConstructorInfo));
            il.NewArray(ExpressionType.Bare, 0);

            il.Call(ExpressionType.New);
        }
    }
}
