﻿using Mono.Cecil;
using Mono.Cecil.Cil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor.Builders
{
    public static class PushBuilder
    {
        private static Dictionary<TypeCode, Func<object, Instruction>> types = new Dictionary<TypeCode, Func<object, Instruction>>
        {
                {TypeCode.Boolean		  , v=>Instruction.Create(OpCodes.Ldc_I4, (bool)v ? 1:0)},
                {TypeCode.Byte            , v=>Instruction.Create(OpCodes.Ldc_I4, (int)(byte)v)},
                {TypeCode.Char            , v=>Instruction.Create(OpCodes.Ldc_I4, (char)v)},                                                
                {TypeCode.Double          , v=>Instruction.Create(OpCodes.Ldc_R8, (double)v)},
                {TypeCode.Int16           , v=>Instruction.Create(OpCodes.Ldc_I4, (short)v)},
                {TypeCode.Int32           , v=>Instruction.Create(OpCodes.Ldc_I4, (int)v)},
                {TypeCode.Int64           , v=>Instruction.Create(OpCodes.Ldc_I8, (long)v)},
                {TypeCode.SByte           , v=>Instruction.Create(OpCodes.Ldc_I4_S, (sbyte)v)},
                {TypeCode.Single          , v=>Instruction.Create(OpCodes.Ldc_R4, (Single)v)},
                {TypeCode.String          , v=>Instruction.Create(OpCodes.Ldstr, (string)v)},
                {TypeCode.UInt16          , v=>Instruction.Create(OpCodes.Ldc_I4, (ushort)v)},
                {TypeCode.UInt32          , v=>Instruction.Create(OpCodes.Ldc_I8, (uint)v)},                
        };

        public static Instruction For(object value)
        {
            var code = Type.GetTypeCode(value.GetType());

            if (types.ContainsKey(code))
            {
                return types[code](value);
            }
            else
            {
                throw new InvalidOperationException(string.Format("Unable to push {0} of type {1} ({2})", value, value.GetType().FullName, code));
            }
        }
    }
}
