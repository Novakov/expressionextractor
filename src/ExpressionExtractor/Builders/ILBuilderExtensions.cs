﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor.Builders
{
    public static class ILBuilderExtensions
    {
        public static ExpressionBuilder Expression(this ILBuilder @this)
        {
            return @this.GetExtensions<ExpressionBuilder>(il => new ExpressionBuilder(il));
        }
    }
}
