﻿using Mono.Cecil;
using Mono.Cecil.Cil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor.Builders
{
    public class ILBuilder
    {
        private static readonly MethodInfo GetTypeFromHandle = ReflectionHelper.GetMethod(() => Type.GetTypeFromHandle(new RuntimeTypeHandle()));
        private static readonly MethodInfo GetMethodFromHandle = ReflectionHelper.GetMethod(() => MethodInfo.GetMethodFromHandle(new RuntimeMethodHandle()));
        private static readonly MethodInfo GetFieldFromHandle = ReflectionHelper.GetMethod(() => FieldInfo.GetFieldFromHandle(new RuntimeFieldHandle()));

        public ModuleDefinition Module { get; private set; }

        public List<Instruction> Instructions { get; set; }

        private Dictionary<Type, object> extensions;

        public ILBuilder(ModuleDefinition module)
        {
            this.Module = module;
            this.Instructions = new List<Instruction>();

            this.extensions = new Dictionary<Type, object>();
        }

        public TExtension GetExtensions<TExtension>(Func<ILBuilder, TExtension> factory)
        {
            if (this.extensions.ContainsKey(typeof(TExtension)))
            {
                return (TExtension)this.extensions[typeof(TExtension)];
            }
            else
            {
                var ext = factory(this);
                this.extensions[typeof(TExtension)] = ext;

                return ext;
            }
        }

        public void Add(Instruction instruction)
        {
            this.Instructions.Add(instruction);
        }

        public void Add(IEnumerable<Instruction> instructions)
        {
            this.Instructions.AddRange(instructions);
        }

        public void Push(object value)
        {
            if (value == null)
            {
                Add(Instruction.Create(OpCodes.Ldnull));
            }
            else if (value is VariableDefinition)
            {
                Add(Instruction.Create(OpCodes.Ldloc, (VariableDefinition)value));
            }
            else if (value is FieldReference)
            {
                Push((FieldReference)value);
            }
            else if (value is ParameterDefinition)
            {
                Add(Instruction.Create(OpCodes.Ldarg, (ParameterDefinition)value));
            }
            else if (value is UInt64)
            {
                Add(Instruction.Create(OpCodes.Ldc_R4, (ulong)value));
                Add(Instruction.Create(OpCodes.Conv_I8));
            }
            else if (value is UInt32)
            {
                Add(Instruction.Create(OpCodes.Ldc_I8, (long)(uint)value));
                Add(Instruction.Create(OpCodes.Conv_I4));
            }
            else if (value is decimal)
            {
                throw new InvalidOperationException("Unable to extract decimal value");
            }
            else
            {
                Add(PushBuilder.For(value));
            }
            //else
            //{
            //    throw new InvalidOperationException("Do not know how to push " + value + " of type " + value.GetType().ToString());
            //}
        }

        private void Push(FieldReference field)
        {
            if (field.Resolve().IsStatic)
            {
                Add(Instruction.Create(OpCodes.Ldsfld, this.Module.Import(field)));
            }
            else
            {
                Add(Instruction.Create(OpCodes.Ldfld, this.Module.Import(field)));
            }
        }

        public void Pop(object target)
        {
            if (target is FieldReference)
            {
                Add(Instruction.Create(OpCodes.Stsfld, (FieldReference)target));
            }
            else if (target is VariableDefinition)
            {
                Add(Instruction.Create(OpCodes.Stloc, (VariableDefinition)target));
            }
            else
            {
                throw new InvalidOperationException("Do not know how to pop " + target + " of type " + target.GetType().ToString());
            }
        }

        public void Box(TypeReference type)
        {
            Add(Instruction.Create(OpCodes.Box, type));
        }

        public void Call(MethodInfo method)
        {
            Add(Instruction.Create(OpCodes.Call, this.Module.Import(method)));
        }

        public void Call(MethodReference method)
        {
            Add(Instruction.Create(OpCodes.Call, this.Module.Import(method)));
        }

        public void NewArray(TypeReference elementType, int size)
        {
            Add(Instruction.Create(OpCodes.Ldc_I4, size));
            Add(Instruction.Create(OpCodes.Newarr, this.Module.Import(elementType)));
        }

        public void NewArray(Type elementType, int size)
        {
            NewArray(this.Module.Import(elementType), size);
        }

        public void StoreArrayElem(int index, object value)
        {
            Add(Instruction.Create(OpCodes.Ldc_I4, index));

            Push(value);

            Add(Instruction.Create(OpCodes.Stelem_Ref));
        }

        public void PushType(Type type)
        {
            PushType(this.Module.Import(type));
        }

        public void PushType(TypeReference type)
        {
            Add(Instruction.Create(OpCodes.Ldtoken, (TypeReference)type));
            Call(GetTypeFromHandle);
        }

        public void Dup()
        {
            Add(Instruction.Create(OpCodes.Dup));
        }

        public void PushMethod(MethodReference getter)
        {
            Add(Instruction.Create(OpCodes.Ldtoken, getter));
            Call(GetMethodFromHandle);
        }

        public void PushMethodInfo(MethodReference method)
        {
            if (method == null)
            {
                Push(null);
            }
            else
            {
                PushMethod(method);
                CastClass(typeof(MethodInfo));
            }
        }

        public void CastClass(Type type)
        {
            Add(Instruction.Create(OpCodes.Castclass, this.Module.Import(type)));
        }

        public void PushFieldInfo(FieldReference field)
        {
            Add(Instruction.Create(OpCodes.Ldtoken, field));
            Call(GetFieldFromHandle);
        }
    }
}
