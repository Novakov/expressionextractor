﻿using ExpressionExtractor.Builders;
using ICSharpCode.NRefactory.CSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ICSharpCode.Decompiler.Ast;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Cecil.Rocks;
using ICSharpCode.Decompiler.ILAst;
using System.Reflection;
using Linq = System.Linq.Expressions;

namespace ExpressionExtractor
{
    class BuilderVisitor : DepthFirstAstVisitor
    {
        private readonly ILBuilder il;
        private readonly MethodDefinition method;
        private readonly FieldDefinition targetField;
        private readonly ModuleDefinition module;

        private VariableDefinition selfParam;

        private Dictionary<string, VariableDefinition> parameters;

        public List<VariableDefinition> Variables { get; private set; }

        public IEnumerable<Instruction> Instructions
        {
            get { return this.il.Instructions; }
        }

        public BuilderVisitor(MethodDefinition method, FieldDefinition targetField)
        {
            this.method = method;
            this.targetField = targetField;

            this.module = this.method.Module;

            this.il = new ILBuilder(this.module);

            this.Variables = new List<VariableDefinition>();
        }

        public override void VisitPrimitiveExpression(PrimitiveExpression primitiveExpression)
        {
            var type = primitiveExpression.Annotation<TypeInformation>().InferredType;

            this.il.Expression().Constant(type, primitiveExpression.Value);

            base.VisitPrimitiveExpression(primitiveExpression);
        }

        public override void VisitThisReferenceExpression(ThisReferenceExpression thisReferenceExpression)
        {
            this.il.Push(this.selfParam);
        }

        public override void VisitMemberReferenceExpression(MemberReferenceExpression memberReferenceExpression)
        {
            memberReferenceExpression.Target.AcceptVisitor(this);

            if (memberReferenceExpression.IsGetterCall())
            {
                var getter = memberReferenceExpression.Annotation<MethodReference>();

                il.Expression().Property(getter);
            }

            if (memberReferenceExpression.IsFieldReference())
            {
                il.Expression().Field(memberReferenceExpression.Annotation<FieldReference>());
            }
        }

        public override void VisitBinaryOperatorExpression(BinaryOperatorExpression binaryOperatorExpression)
        {
            binaryOperatorExpression.Left.AcceptVisitor(this);
            binaryOperatorExpression.Right.AcceptVisitor(this);

            var method = OperatorMapping.For(binaryOperatorExpression.Operator);
            switch (binaryOperatorExpression.Operator)
            {
                case BinaryOperatorType.Equality:
                case BinaryOperatorType.GreaterThan:
                case BinaryOperatorType.GreaterThanOrEqual:
                case BinaryOperatorType.LessThan:
                case BinaryOperatorType.LessThanOrEqual:
                case BinaryOperatorType.InEquality:
                    il.Call(method);
                    break;

                case BinaryOperatorType.Add:
                case BinaryOperatorType.BitwiseAnd:
                case BinaryOperatorType.BitwiseOr:
                case BinaryOperatorType.ConditionalAnd:
                case BinaryOperatorType.ConditionalOr:
                case BinaryOperatorType.Divide:
                case BinaryOperatorType.ExclusiveOr:
                case BinaryOperatorType.Modulus:
                case BinaryOperatorType.Multiply:
                case BinaryOperatorType.ShiftLeft:
                case BinaryOperatorType.ShiftRight:
                case BinaryOperatorType.Subtract:
                    il.Call(method);
                    break;

                case BinaryOperatorType.NullCoalescing:
                    this.il.Call(method);
                    break;

                default:
                    throw new InvalidOperationException("Unable to binary decompose operator");
            }
        }

        public override void VisitUnaryOperatorExpression(UnaryOperatorExpression unaryOperatorExpression)
        {
            unaryOperatorExpression.Expression.AcceptVisitor(this);

            switch (unaryOperatorExpression.Operator)
            {
                case UnaryOperatorType.BitNot:
                case UnaryOperatorType.Not:
                    il.Expression().Not();
                    break;
                case UnaryOperatorType.Minus:
                    il.Expression().Negate();
                    break;
                default:
                    throw new InvalidOperationException("Unable to unary decompose operator");
            }
        }

        public override void VisitCastExpression(CastExpression castExpression)
        {
            castExpression.Expression.AcceptVisitor(this);
            il.Expression().Convert(castExpression.Annotation<TypeInformation>().InferredType);
        }

        public override void VisitNullReferenceExpression(NullReferenceExpression nullReferenceExpression)
        {
            var z = nullReferenceExpression.Ancestors.Select(x => x.Annotation<TypeInformation>()).ToArray();
            var type = z.First(x => x != null && x.InferredType != null);

            il.Expression().Constant(type.InferredType, null);
        }

        public override void VisitInvocationExpression(InvocationExpression invocationExpression)
        {
            invocationExpression.Target.AcceptVisitor(this);

            il.PushMethodInfo(invocationExpression.Annotation<MethodReference>());

            il.NewArray(ExpressionType.Bare, invocationExpression.Arguments.Count);

            foreach (var item in invocationExpression.Arguments.Select((x, i) => new { Expr = x, Index = i }))
            {
                il.Dup();
                il.Push(item.Index);
                item.Expr.AcceptVisitor(this);
                il.Add(Instruction.Create(OpCodes.Stelem_Ref));
            }

            il.Expression().Call();
        }

        public override void VisitIdentifierExpression(IdentifierExpression identifierExpression)
        {
            var variable = identifierExpression.Annotation<ILVariable>();

            if (variable != null)
            {
                if (variable.IsParameter)
                {
                    il.Push(this.parameters[variable.OriginalParameter.Name]);
                }
            }
            else
            {
                base.VisitIdentifierExpression(identifierExpression);
            }
        }

        public override void VisitTypeReferenceExpression(TypeReferenceExpression typeReferenceExpression)
        {
            il.Push(null);
        }

        public override void VisitObjectCreateExpression(ObjectCreateExpression objectCreateExpression)
        {
            il.PushMethod(objectCreateExpression.Annotation<MethodReference>());
            il.CastClass(typeof(ConstructorInfo));
            il.NewArray(ExpressionType.Bare, objectCreateExpression.Arguments.Count);

            foreach (var item in objectCreateExpression.Arguments.Select((x, i) => new { Expr = x, Index = i }))
            {
                il.Dup();
                il.Push(item.Index);
                item.Expr.AcceptVisitor(this);
                il.Add(Instruction.Create(OpCodes.Stelem_Ref));
            }

            il.Call(ExpressionType.New);

            objectCreateExpression.Initializer.AcceptVisitor(new Visitors.ObjectInitializerVisitor(this.il, this));
        }

        public override void VisitArrayCreateExpression(ArrayCreateExpression arrayCreateExpression)
        {
            if (arrayCreateExpression.Initializer.IsNull)
            {
                var elementType = arrayCreateExpression.Annotation<TypeInformation>().InferredType.DecomposeArrayType(arrayCreateExpression.Arguments.Count);
                il.PushType(elementType);

                il.NewArray(ExpressionType.Bare, arrayCreateExpression.Arguments.Count);

                foreach (var item in arrayCreateExpression.Arguments.AsIndexed())
                {
                    il.Dup();
                    il.Push(item.Index);

                    item.Value.AcceptVisitor(this);

                    il.Add(Instruction.Create(OpCodes.Stelem_Ref));
                }

                il.Call(ExpressionType.NewArrayBounds);
            }
            else
            {
                var elementType = arrayCreateExpression.Annotation<TypeInformation>().InferredType.DecomposeArrayType(1);
                il.PushType(elementType);

                il.NewArray(ExpressionType.Bare, arrayCreateExpression.Initializer.Elements.Count);

                arrayCreateExpression.Initializer.AcceptVisitor(new Visitors.ArrayInitializerVisitor(this.il, this));

                il.Call(ExpressionType.NewArrayInit);
            }
        }

        public override void VisitReturnStatement(ReturnStatement returnStatement)
        {
            this.selfParam = new VariableDefinition(il.Module.Import(ExpressionType.ParameterType));

            this.Variables.Add(this.selfParam);

            il.Expression().Parameter(this.method.DeclaringType, "self");

            il.Pop(this.selfParam);

            this.parameters = new Dictionary<string, VariableDefinition>();

            foreach (var item in this.method.Parameters)
            {
                var def = new VariableDefinition(this.module.Import(ExpressionType.ParameterType));
                this.Variables.Add(def);

                il.Expression().Parameter(item.ParameterType, item.Name);
                il.Pop(def);

                this.parameters[item.Name] = def;
            }

            returnStatement.Expression.AcceptVisitor(this);

            il.NewArray(ExpressionType.ParameterType, parameters.Count + 1);

            var arrayVar = new VariableDefinition(il.Module.Import(ExpressionType.ParameterType).MakeArrayType());
            this.Variables.Add(arrayVar);

            il.Pop(arrayVar);

            il.Push(arrayVar);
            il.StoreArrayElem(0, selfParam);

            foreach (var item in this.parameters.Select((x, i) => new { Var = x.Value, Index = i }))
            {
                il.Push(arrayVar);
                il.StoreArrayElem(item.Index + 1, item.Var);
            }

            il.Push(arrayVar);
            il.Call(GetLambdaCall());

            il.Pop(targetField);
        }

        private MethodReference GetLambdaCall()
        {
            var method = new GenericInstanceMethod(this.il.Module.Import(ExpressionType.LambdaMethod));

            method.GenericArguments.Add((this.targetField.FieldType as GenericInstanceType).GenericArguments[0]);

            return method;
        }
    }
}
