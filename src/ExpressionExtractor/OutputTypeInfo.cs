﻿using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor
{
    class OutputTypeInfo
    {
        public MethodDefinition Initializer { get; private set; }

        public TypeDefinition Definition { get; private set; }

        public OutputTypeInfo(TypeDefinition definition, MethodDefinition initializer)
        {
            this.Definition = definition;
            this.Initializer = initializer;
        }
    }
}
