﻿using ExpressionExtractor.Builders;
using ICSharpCode.NRefactory.CSharp;
using Mono.Cecil.Cil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor.Visitors
{
    public class ArrayInitializerVisitor : DepthFirstAstVisitor
    {
        private readonly ILBuilder il;
        private readonly IAstVisitor baseVisitor;

        public ArrayInitializerVisitor(ILBuilder il, IAstVisitor baseVisitor)
        {
            this.il = il;
            this.baseVisitor = baseVisitor;
        }

        public override void VisitArrayInitializerExpression(ArrayInitializerExpression arrayInitializerExpression)
        {
            foreach (var item in arrayInitializerExpression.Elements.AsIndexed())
            {
                il.Dup();
                il.Push(item.Index);

                item.Value.AcceptVisitor(this.baseVisitor);

                il.Add(Instruction.Create(OpCodes.Stelem_Ref));
            }
        }
    }
}
