﻿using ExpressionExtractor.Builders;
using ICSharpCode.NRefactory.CSharp;
using Mono.Cecil;
using Mono.Cecil.Cil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Linq = System.Linq.Expressions;

namespace ExpressionExtractor.Visitors
{
    public class ObjectInitializerVisitor : DepthFirstAstVisitor
    {
        private readonly ILBuilder il;
        private readonly IAstVisitor baseVisitor;

        public ObjectInitializerVisitor(ILBuilder il, IAstVisitor baseVisitor)
        {
            this.il = il;
            this.baseVisitor = baseVisitor;
        }

        public override void VisitArrayInitializerExpression(ArrayInitializerExpression arrayInitializerExpression)
        {
            il.NewArray(typeof(Linq.MemberBinding), arrayInitializerExpression.Elements.Count);

            foreach (var item in arrayInitializerExpression.Elements.Select((x, i) => new { Expr = x, Index = i }))
            {
                il.Dup();
                il.Push(item.Index);
                item.Expr.AcceptVisitor(this);
                il.Add(Instruction.Create(OpCodes.Stelem_Ref));
            }

            il.Call(ExpressionType.MemberInit);
        }

        public override void VisitNamedExpression(NamedExpression namedExpression)
        {
            if (namedExpression.Annotation<PropertyReference>() != null)
            {
                il.PushMethodInfo(namedExpression.Annotation<MethodReference>());
            }
            if (namedExpression.Annotation<FieldReference>() != null)
            {
                il.PushFieldInfo(namedExpression.Annotation<FieldReference>());
            }

            namedExpression.Expression.AcceptVisitor(baseVisitor);

            il.Call(ExpressionType.Bind);
        }
    }
}
