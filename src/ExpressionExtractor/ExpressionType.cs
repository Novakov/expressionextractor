﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor
{
    static class ExpressionType
    {
        public static readonly Type Bare = typeof(Expression);

        public static readonly Type ParameterType = typeof(ParameterExpression);

        public static readonly MethodInfo LambdaMethod = ReflectionHelper.GetMethod(() => Expression.Lambda<Func<int, int>>(null, null)).GetGenericMethodDefinition();

        public static readonly MethodInfo Constant = ReflectionHelper.GetMethod(() => Expression.Constant(Is<object>(), Is<Type>()));

        public static readonly MethodInfo Parameter = ReflectionHelper.GetMethod(() => Expression.Parameter(typeof(object), Is<string>()));

        public static readonly MethodInfo Property = ReflectionHelper.GetMethod(() => Expression.Property(null, Is<MethodInfo>()));

        public static readonly MethodInfo Add = ReflectionHelper.GetMethod(() => Expression.Add(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo And = ReflectionHelper.GetMethod(() => Expression.And(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo Divide = ReflectionHelper.GetMethod(() => Expression.Divide(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo ExclusiveOr = ReflectionHelper.GetMethod(() => Expression.ExclusiveOr(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo LeftShift = ReflectionHelper.GetMethod(() => Expression.LeftShift(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo Modulo = ReflectionHelper.GetMethod(() => Expression.Modulo(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo Multiply = ReflectionHelper.GetMethod(() => Expression.Multiply(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo Or = ReflectionHelper.GetMethod(() => Expression.Or(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo RightShift = ReflectionHelper.GetMethod(() => Expression.RightShift(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo Subtract = ReflectionHelper.GetMethod(() => Expression.Subtract(Is<Expression>(), Is<Expression>()));

        public static readonly MethodInfo Equal = ReflectionHelper.GetMethod(() => Expression.Equal(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo GreaterThanOrEqual = ReflectionHelper.GetMethod(() => Expression.GreaterThanOrEqual(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo GreaterThan = ReflectionHelper.GetMethod(() => Expression.GreaterThan(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo LessThan = ReflectionHelper.GetMethod(() => Expression.LessThan(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo LessThanOrEqual = ReflectionHelper.GetMethod(() => Expression.LessThanOrEqual(Is<Expression>(), Is<Expression>()));
        public static readonly MethodInfo NotEqual = ReflectionHelper.GetMethod(() => Expression.NotEqual(Is<Expression>(), Is<Expression>()));

        public static readonly MethodInfo Coalesce = ReflectionHelper.GetMethod(() => Expression.Coalesce(Is<Expression>(), Is<Expression>()));

        public static readonly MethodInfo Negate = ReflectionHelper.GetMethod(() => Expression.Negate(Is<Expression>()));
        public static readonly MethodInfo Not = ReflectionHelper.GetMethod(() => Expression.Not(Is<Expression>()));

        public static readonly MethodInfo Convert = ReflectionHelper.GetMethod(() => Expression.Convert(Is<Expression>(), Is<Type>()));

        public static readonly MethodInfo Call = ReflectionHelper.GetMethod(() => Expression.Call(Is<Expression>(), Is<MethodInfo>(), Is<Expression[]>()));

        public static readonly MethodInfo Field = ReflectionHelper.GetMethod(() => Expression.Field(Is<Expression>(), Is<FieldInfo>()));

        public static readonly MethodInfo New = ReflectionHelper.GetMethod(() => Expression.New(Is<ConstructorInfo>(), Is<Expression[]>()));

        public static readonly MethodInfo MemberInit = ReflectionHelper.GetMethod(() => Expression.MemberInit(Is<NewExpression>(), Is<MemberBinding[]>()));

        public static readonly MethodInfo Bind = ReflectionHelper.GetMethod(() => Expression.Bind(Is<MethodInfo>(), Is<Expression>()));

        public static readonly MethodInfo NewArrayBounds = ReflectionHelper.GetMethod(() => Expression.NewArrayBounds(Is<Type>(), Is<Expression[]>()));

        public static readonly MethodInfo NewArrayInit = ReflectionHelper.GetMethod(() => Expression.NewArrayInit(Is<Type>(), Is<Expression[]>()));

        private static T Is<T>()
        {
            return default(T);
        }
    }
}
