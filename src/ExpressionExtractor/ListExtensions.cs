﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor
{
    static class ListExtensions
    {
        public static void AddRange<T>(this Mono.Collections.Generic.Collection<T> @this, IEnumerable<T> elements)
        {
            foreach (var item in elements)
            {
                @this.Add(item);
            }
        }
    }
}
