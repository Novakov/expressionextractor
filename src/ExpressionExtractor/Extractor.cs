﻿using Mono.Cecil;
using Mono.Cecil.Cil;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace ExpressionExtractor
{
    public class Extractor
    {
        private string input;
        private string output;
        private AssemblyDefinition assembly;        

        private Dictionary<TypeDefinition, OutputTypeInfo> typeMapping;

        public ILog Log { get; set; }

        private TypeSystem TypeSystem { get { return this.assembly.MainModule.TypeSystem; } }

        public Extractor(string Input, string Output)
        {
            this.input = Input;
            this.output = Output;

            this.typeMapping = new Dictionary<TypeDefinition, OutputTypeInfo>();
        }

        public void Build()
        {
            OpenInputAssembly();            

            foreach (var item in GetInputMethods())
            {
                Log.Info("Input method {0}", item);
                var outputType = GetOutputType(item.DeclaringType);

                DefineExpressionForMethod(item, outputType);
            }

            Save();
        }

        private void DefineExpressionForMethod(MethodDefinition inputMethod, OutputTypeInfo outputType)
        {
            var fieldType = BuildFieldType(inputMethod);

            var field = new FieldDefinition(inputMethod.Name, FieldAttributes.Static | FieldAttributes.Public, fieldType);

            outputType.Definition.Fields.Add(field);

            var builder = new InitializerBuilder(inputMethod, field);

            outputType.Initializer.Body.Instructions.AddRange(builder.Build());
            outputType.Initializer.Body.Variables.AddRange(builder.Variables);
        }

        private TypeReference BuildFieldType(MethodDefinition method)
        {
            Type delegateType = null;
            switch (method.Parameters.Count)
            {
                case 0:
                    delegateType = typeof(Func<,>);
                    break;
                case 1:
                    delegateType = typeof(Func<,,>);
                    break;
                case 2:
                    delegateType = typeof(Func<,,,>);
                    break;
                case 3:
                    delegateType = typeof(Func<,,,,>);
                    break;
                case 4:
                    delegateType = typeof(Func<,,,,,>);
                    break;
                case 5:
                    delegateType = typeof(Func<,,,,,,>);
                    break;
                case 6:
                    delegateType = typeof(Func<,,,,,,,>);
                    break;
                case 7:
                    delegateType = typeof(Func<,,,,,,,,>);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("Too many argument for extracted method " + method.ToString());
            }

            var type = new GenericInstanceType(this.assembly.MainModule.Import(delegateType));
            type.GenericArguments.Add(this.assembly.MainModule.Import(method.DeclaringType));

            foreach (var item in method.Parameters)
            {
                type.GenericArguments.Add(this.assembly.MainModule.Import(item.ParameterType));
            }

            type.GenericArguments.Add(this.assembly.MainModule.Import(method.ReturnType));

            var expressionType = new GenericInstanceType(this.assembly.MainModule.Import(typeof(Expression<>)))
            {
                GenericArguments =
                {
                    type
                }
            };

            return expressionType;
        }

        private OutputTypeInfo GetOutputType(TypeDefinition inputType)
        {
            if (typeMapping.ContainsKey(inputType))
            {
                return typeMapping[inputType];
            }

            var outputType = CreateType(inputType);

            typeMapping.Add(inputType, outputType);

            return outputType;
        }

        private OutputTypeInfo CreateType(TypeDefinition inputType)
        {
            var type = new TypeDefinition(inputType.Namespace + ".Expr", inputType.Name, TypeAttributes.Public | TypeAttributes.Abstract | TypeAttributes.Sealed);

            type.BaseType = TypeSystem.Object;

            var cctor = new MethodDefinition(".cctor", MethodAttributes.RTSpecialName | MethodAttributes.SpecialName | MethodAttributes.Static | MethodAttributes.HideBySig | MethodAttributes.Private, TypeSystem.Void);
            cctor.Body.InitLocals = true;

            type.Methods.Add(cctor);

            this.assembly.MainModule.Types.Add(type);

            return new OutputTypeInfo(type, cctor);
        }

        private IEnumerable<MethodDefinition> GetInputMethods()
        {
            foreach (var method in this.assembly.MainModule.Types.SelectMany(x => x.Methods).ToList())
            {
                if (method.CustomAttributes.Any(x => x.AttributeType.Name == "ExtractAttribute"))
                {
                    yield return method;
                }
            }
        }

        private void Save()
        {
            foreach (var item in typeMapping)
            {
                item.Value.Initializer.Body.Instructions.Add(Instruction.Create(OpCodes.Ret));
            }

            this.assembly.Write(this.output);
        }

        private void OpenInputAssembly()
        {
            this.assembly = AssemblyDefinition.ReadAssembly(input);
        }

        private void CreateOutputAssembly()
        {            
            var baseName = Path.GetFileNameWithoutExtension(input);
            var asmName = new AssemblyNameDefinition(baseName, new Version());
            this.assembly = AssemblyDefinition.CreateAssembly(asmName, baseName, ModuleKind.Dll);
        }
    }
}
