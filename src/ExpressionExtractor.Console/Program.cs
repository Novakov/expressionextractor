﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 2)
            {
                var p = System.Diagnostics.Process.Start(@"C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe", @"D:\Coding\MethodToExpression\src\SampleDomain\SampleDomain.csproj /p:RunExtract=False /t:Rebuild");
                p.WaitForExit();
            }

            var file = args[0];
            var extractor = new Extractor(file, Path.ChangeExtension(file, ".Expr.dll"));

            extractor.Log = new ConsoleLogger();

            extractor.Build();
        }
    }
}
