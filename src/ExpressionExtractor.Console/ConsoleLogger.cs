﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionExtractor.Console
{
    class ConsoleLogger : ILog
    {
        public void Info(string text, params object[] args)
        {
            System.Console.WriteLine(text, args);
        }
    }
}
