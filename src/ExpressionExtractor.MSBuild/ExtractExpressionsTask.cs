﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ExpressionExtractor.MSBuild
{
    public class ExtractExpressionsTask : Task
    {
        [Required]
        public string Input { get; set; }

        [Required]
        public string Output { get; set; }

        public override bool Execute()
        {
            var setup = new AppDomainSetup
            {
                ApplicationBase = Path.GetDirectoryName(typeof(ExtractExpressionsTask).Assembly.Location)
            };

            var domain = AppDomain.CreateDomain("ExtractExpression", null, setup);

            var executor = domain.CreateInstanceAndUnwrap(typeof(Executor).Assembly.FullName, typeof(Executor).FullName) as Executor;

            executor.Execute(Input, Output, this.Log);

            AppDomain.Unload(domain);            

#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                System.Diagnostics.Debugger.Break();
            }
#endif

            return true;
        }
    }
}
