﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExpressionExtractor.MSBuild
{
    class MSBuildLogger : ILog
    {
        private TaskLoggingHelper taskLoggingHelper;

        public MSBuildLogger(TaskLoggingHelper taskLoggingHelper)
        {            
            this.taskLoggingHelper = taskLoggingHelper;
        }

        public void Info(string text, params object[] args)
        {
            this.taskLoggingHelper.LogMessage(MessageImportance.Normal, string.Format(text, args));
        }
    }
}
