﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionExtractor.MSBuild
{
    class Executor : MarshalByRefObject
    {
        internal void Execute(string input, string output, Microsoft.Build.Utilities.TaskLoggingHelper taskLoggingHelper)
        {
            var extractor = new Extractor(input, output);
            extractor.Log = new MSBuildLogger(taskLoggingHelper);


            extractor.Build();
        }
    }
}
