﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SampleDomain
{
    class Sandbox
    {
        static void Test()
        {
            Expression<Func<int[]>> ex0 = () => new int[0];
            Expression<Func<int[]>> ex1 = () => new int[1];
            Expression<Func<int[]>> ex2 = () => new[] { 1, 2, 3 };
        }
    }
}
