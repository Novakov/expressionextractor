﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDomain
{
    public class Person
    {
        public string Name { get; set; }

        public string City { get; set; }

        public DateTime BirthAt { get; set; }

        public MaritalStatus MaritalStatus { get; set; }

        [Extract]
        public bool IsAdult(DateTime asof)
        {
            return this.Age(asof) > 18;
        }

        public int A(int x)
        {
            return 5;
        }

        [Extract]
        public double Age(DateTime asof)
        {
            return (asof - this.BirthAt).TotalDays / 365.0;
        }

        public bool NeverMarried
        {
            [Extract]
            get { return this.MaritalStatus == MaritalStatus.Single; }
        }

        public bool IsFree
        {
            [Extract]
            get { return this.MaritalStatus != MaritalStatus.Married; }
        }
    }
}
