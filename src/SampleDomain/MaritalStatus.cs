﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleDomain
{
    public enum MaritalStatus
    {
        Single,
        Married,
        Divorced,
        Widowed
    }
}
