﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDomain
{
    public class PrimitiveTypes
    {
        [Extract]
        public Boolean GetBoolean()
        {
            return true;
        }

        [Extract]
        public Byte GetByte()
        {
            return 17;
        }

        [Extract]
        public Char GetChar()
        {
            return 'A';
        }

        [Extract]
        public Double GetDouble()
        {
            return -5.6;
        }

        [Extract]
        public Int16 GetInt16()
        {
            return -5000;
        }

        [Extract]
        public Int32 GetInt32()
        {
            return -2000000;
        }

        [Extract]
        public Int64 GetInt64()
        {
            return -4000000000;
        }

        [Extract]
        public Object GetNullObject()
        {
            return null;
        }

        [Extract]
        public Object GetObject()
        {
            return default(Object);
        }

        [Extract]
        public SByte GetSByte()
        {
            return -100;
        }

        [Extract]
        public Single GetSingle()
        {
            return -1.2f;
        }

        [Extract]
        public String GetString()
        {
            return "Hello World";
        }

        [Extract]
        public string GetNullString()
        {
            return null;
        }

        [Extract]
        public UInt16 GetUInt16()
        {
            return 65500;
        }

        [Extract]
        public UInt32 GetUInt32()
        {
            return 4000000;
        }

        [Extract]
        public UInt64 GetUInt64()
        {
            return 8000000000;
        }
    }
}
