﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDomain
{
    public class Steps
    {
        public int field;

        public int? nullbleInt;

        public int Age { get; set; }

        public string Name { get; set; }

        public bool Citizen { get; set; }

        public static int StaticProp { get; set; }

        public static int staticField;

        public static int StaticMethod(int x)
        {
            return 4;
        }

        [Extract]
        public int Constant()
        {
            return 5;
        }

        [Extract]
        public int PropertyReference()
        {
            return this.Age;
        }

        [Extract]
        public int IndirectPropertyReference()
        {
            return this.Name.Length;
        }

        [Extract]
        public int BinaryOperator()
        {
            return this.Age + 5;
        }

        [Extract]
        public bool UnaryOperator()
        {
            return !this.Citizen;
        }

        [Extract]
        public int MinusAge()
        {
            return -this.Age;
        }

        [Extract]
        public int AgePlusOne()
        {
            return this.Age + 1;
        }

        [Extract]
        public int FieldReference()
        {
            return this.field;
        }

        [Extract]
        public int StaticPropReference()
        {
            return Steps.StaticProp;
        }

        [Extract]
        public int StaticFieldReference()
        {
            return Steps.staticField;
        }

        [Extract]
        public int StaticMethodReference()
        {
            return Steps.StaticMethod(5);
        }

        [Extract]
        public int Coalesce()
        {
            return (this.nullbleInt ?? 7) + 2;
        }
    }
}
