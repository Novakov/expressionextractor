﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDomain
{
    public class CtorCalls
    {
        public int X { get; set; }
        public int Y { get; set; }

        public CtorCalls Sub { get; set; }

        public CtorCalls(int x)
        {

        }

        [Extract]
        public object ParamlessCtor()
        {
            return new object();
        }

        [Extract]
        public CtorCalls CtorWithParam()
        {
            return new CtorCalls(66);
        }

        [Extract]
        public CtorCalls CtorWithInitializer()
        {
            return new CtorCalls(66) { X = 77 };
        }

        [Extract]
        public CtorCalls CtorWithInitializerForTwoProperties()
        {
            return new CtorCalls(66) { X = 77, Y = 6 };
        }

        [Extract]
        public CtorCalls CtorWithNestedInitializer()
        {
            return new CtorCalls(66)
            {
                X = 77,
                Y = 6,
                Sub = new CtorCalls(99)
                {
                    X = 5,
                    Y = 7
                }
            };
        }
    }
}
