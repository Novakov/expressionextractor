﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleDomain
{
    public class Arrays
    {
        [Extract]
        public int[] ZeroSizeArray()
        {
            return new int[0];
        }

        [Extract]
        public int[] NonZeroSizeArray()
        {
            return new int[5];
        }

        [Extract]
        public int[] ArrayOfPrimitiveTypes()
        {
            return new int[] { 1, 2, 3, 4, 5 };
        }

        [Extract]
        public object[] ArrayOfReferenceTypes()
        {
            return new object[] { new object(), new object() };
        }

        [Extract]
        public object[] ArrayOfReferenceTypesWithInitializer()
        {
            return new object[] { new object(), new CtorCalls(66) { X = 2 } };
        }

        [Extract]
        public object[] ArrayOfReferenceTypesWithNestedInitializer()
        {
            return new object[] 
            { 
                new object(), 
                new CtorCalls(66) 
                { 
                    X = 2, 
                    Sub = new CtorCalls(77) 
                    { 
                        X = 88 
                    } 
                } 
            };
        }
    }
}
