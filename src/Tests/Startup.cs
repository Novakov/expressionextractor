﻿using ExpressionExtractor;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Tests;

[SetUpFixture]
public class Startup
{
    private string sampleDomainDll;
    private string sampleDomainBak;

    [SetUp]
    public void Compile()
    {
        var location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        sampleDomainDll = Path.GetFullPath("SampleDomain.dll");
        sampleDomainBak = Path.ChangeExtension(sampleDomainDll, ".bak");

        if (File.Exists(sampleDomainBak))
        {
            File.Delete(sampleDomainBak);
        }

        File.Move(sampleDomainDll, sampleDomainBak);

        var targetFile = Path.Combine(TestContext.CurrentContext.WorkDirectory, "SampleDomain.Expr.dll");

        if (File.Exists(targetFile))
        {
            File.Delete(targetFile);
        }

        var extractor = new Extractor(sampleDomainBak, targetFile);

        extractor.Log = new DummyLog();

        extractor.Build();

        var sampleDomain = Assembly.LoadFrom(targetFile);

        AppDomain.CurrentDomain.AssemblyResolve += (s, e) =>
            {
                if (e.Name.Contains("SampleDomain"))
                {
                    return sampleDomain;
                }

                return null;
            };
    }

    [TearDown]
    public void RevertSampleDomain()
    {
        File.Move(sampleDomainBak, sampleDomainDll);
    }
}

