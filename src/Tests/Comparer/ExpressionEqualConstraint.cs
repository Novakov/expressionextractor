﻿using NUnit.Framework.Constraints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Comparer
{
    class ExpressionEqualConstraint : Constraint
    {
        private Expression expected;

        public ExpressionEqualConstraint(Expression expected)
        {
            this.expected = expected;
        }

        public override bool Matches(object actual)
        {
            this.actual = actual;
            return ExpressionEqualityComparer.Instance.Equals((Expression)actual, this.expected);
        }

        public override void WriteDescriptionTo(MessageWriter writer)
        {
            writer.WriteExpectedValue(this.expected);      
        }        
    }
}
