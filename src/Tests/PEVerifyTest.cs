﻿using Microsoft.Win32;
using NUnit.Framework;
using SampleDomain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class PEVerifyTest
    {
        private string toolPath;

        [SetUp]
        public void SetUp()
        {
            string sdkPath;

            using (var key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SDKs\Windows\v8.0A", false))
            {
                sdkPath = key.GetValue("InstallationFolder").ToString();
            }

            toolPath = Path.Combine(sdkPath, "bin", "NETFX 4.0 Tools", "PEVerify.exe");
        }

        [Test]
        public void ResultAssemblyShouldBeValid()
        {
            var targetAssemblyLocation = typeof(Person).Assembly.Location;

            var startInfo = new ProcessStartInfo
            {
                FileName = this.toolPath,
                Arguments = string.Format("\"{0}\" /IL /MD /NOLOGO /VERBOSE", targetAssemblyLocation),
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            var p = Process.Start(startInfo);
            p.WaitForExit();

            if (p.ExitCode != 0)
            {
                var errors = p.StandardOutput.ReadToEnd().Split('\n');

                Console.Error.WriteLine("PEVerify errors:");
                Console.Error.Write(string.Join("\n", errors));

                Assert.Fail(errors[errors.Length - 2]);
            }
        }
    }
}
