﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    public static class Asserts
    {
        public static void Equal<T>(Expression<T> expected, Expression<T> actual)
        {
            Assert.That(actual, new Comparer.ExpressionEqualConstraint(expected));
        }

        public static ExpressionEquality<T> That<T>(Expression<T> actual)
        {
            return new ExpressionEquality<T>(actual);
        }

        public class ExpressionEquality<T>
        {
            private Expression<T> actual;

            public ExpressionEquality(Expression<T> actual)
            {
                this.actual = actual;
            }

            public void Matches(Expression<T> expected)
            {
                Asserts.Equal(expected, this.actual);
            }
        }
    }
}
