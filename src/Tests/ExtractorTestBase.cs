﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    public abstract class ExpressionTestBase
    {
        protected void ExpressionEqual<TDelegate>(Expression<TDelegate> member, Expression<TDelegate> expected)
        {
            var expr = Helper.GetExpression<TDelegate>(member);

            Asserts.Equal(expected, expr);
        }
    }
}
