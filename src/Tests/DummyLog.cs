﻿using ExpressionExtractor;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tests
{
    class DummyLog : ILog
    {
        public void Info(string text, params object[] args)
        {
            Console.WriteLine("[EXTRACT] " + text, args);
        }
    }
}
