﻿using NUnit.Framework;
using SampleDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class CtorCallsTest : ExpressionTestBase
    {
        [Test]
        public void ParamlessCtorTest()
        {
            ExpressionEqual<Func<CtorCalls, object>>(s => s.ParamlessCtor(), s => new object());
        }

        [Test]
        public void CtorWithParamTest()
        {
            ExpressionEqual<Func<CtorCalls, CtorCalls>>(s => s.CtorWithParam(), s => new CtorCalls(66));
        }

        [Test]
        public void CtorWithInitializerTest()
        {
            ExpressionEqual<Func<CtorCalls, CtorCalls>>(s => s.CtorWithInitializer(), s => new CtorCalls(66) { X = 77 });
        }

        [Test]
        public void CtorWithInitializerForTwoProperties()
        {
            ExpressionEqual<Func<CtorCalls, CtorCalls>>(s => s.CtorWithInitializerForTwoProperties(), s => new CtorCalls(66) { X = 77, Y = 6 });
        }

        [Test]
        public void CtorWithNestedInitializer()
        {
            ExpressionEqual<Func<CtorCalls, CtorCalls>>(s => s.CtorWithNestedInitializer(), s => new CtorCalls(66)
            {
                X = 77,
                Y = 6,
                Sub = new CtorCalls(99)
                {
                    X = 5,
                    Y = 7
                }
            });
        }
    }
}
