﻿using NUnit.Framework;
using SampleDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class PrimitiveTypesTest
    {
        [Test]
        public void TestBoolean()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, Boolean>>(s => s.GetBoolean());

            Asserts.Equal(p => true, expr);
        }

        [Test]
        public void TestByte()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, Byte>>(s => s.GetByte());

            Asserts.Equal(p => 17, expr);
        }

        [Test]
        public void TestChar()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, Char>>(s => s.GetChar());

            Asserts.Equal(p => 'A', expr);
        }

        [Test]
        public void TestDouble()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, Double>>(s => s.GetDouble());

            Asserts.Equal(p => -5.6, expr);
        }

        [Test]
        public void TestInt16()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, Int16>>(s => s.GetInt16());

            Asserts.Equal(p => -5000, expr);
        }

        [Test]
        public void TestInt32()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, Int32>>(s => s.GetInt32());

            Asserts.Equal(p => -2000000, expr);
        }

        [Test]
        public void TestInt64()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, Int64>>(s => s.GetInt64());

            Asserts.Equal(p => -4000000000, expr);
        }

        [Test]
        public void TestNullObject()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, Object>>(s => s.GetNullObject());

            Asserts.Equal(p => null, expr);
        }

        [Test]
        public void TestSByte()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, SByte>>(s => s.GetSByte());

            Asserts.Equal(p => -100, expr);
        }

        [Test]
        public void TestSingle()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, Single>>(s => s.GetSingle());

            Asserts.Equal(p => -1.2f, expr);
        }

        [Test]
        public void TestString()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, String>>(s => s.GetString());

            Asserts.Equal(p => "Hello World", expr);
        }

        [Test]
        public void TestNullString()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, String>>(s => s.GetNullString());

            Asserts.Equal(p => (string)null, expr);
        }

        [Test]
        public void TestUInt16()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, UInt16>>(s => s.GetUInt16());

            Asserts.Equal(p => 65500, expr);
        }

        [Test]
        public void TestUInt32()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, UInt32>>(s => s.GetUInt32());

            Asserts.Equal(p => 4000000, expr);
        }

        [Test]
        public void TestUInt64()
        {
            var expr = Helper.GetExpression<Func<PrimitiveTypes, UInt64>>(s => s.GetUInt64());

            Asserts.Equal(p => 8000000000, expr);
        }
    }
}
