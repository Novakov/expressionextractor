﻿using ExpressionInliner;
using Moq;
using Moq.Language.Flow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Inline
{
    public static class ResolverMockExtensions
    {
        public static ResolverSetup<TDelegate> ForMember<TDelegate>(this Mock<IMemberResolver> @this, Expression<Func<Target, object>> member)
        {
            var body = member.Body;

            if (body.NodeType == ExpressionType.Convert)
            {
                body = ((UnaryExpression)body).Operand;
            }

            if (body.NodeType == ExpressionType.MemberAccess)
            {
                var property = ((MemberExpression)body).Member;
                return new ResolverSetup<TDelegate>(@this.Setup(x => x.Resolve(It.Is<MemberInfo>(y => y == property))));
            }

            if (body.NodeType == ExpressionType.Call)
            {
                var method = ((MethodCallExpression)body).Method;
                return new ResolverSetup<TDelegate>(@this.Setup(x => x.Resolve(It.Is<MemberInfo>(y => y == method))));
            }

            throw new InvalidOperationException("Cannot setup");
        }

        public class ResolverSetup<TDelegate>
        {
            private ISetup<IMemberResolver, LambdaExpression> setup;

            public ResolverSetup(ISetup<IMemberResolver, LambdaExpression> setup)
            {
                this.setup = setup;
            }

            public void Returns(Expression<TDelegate> expr)
            {
                this.setup.Returns(expr);
            }
        }
    }
}
