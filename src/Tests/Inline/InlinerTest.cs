﻿using ExpressionInliner;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Inline
{
    [TestFixture]
    public class InlinerTest
    {
        private Mock<IMemberResolver> resolver;
        private Inliner inliner;

        [SetUp]
        public void SetUp()
        {
            this.resolver = new Mock<IMemberResolver>();

            this.inliner = new Inliner(this.resolver.Object);
        }

        [Test]
        public void ShouldReturnSameExpressionWhenNoMembersToInline()
        {
            // arrange
            Expression<Func<Target, int>> input = t => t.NotInlined;

            // act
            var output = this.inliner.Inline(input);

            // assert
            Asserts.That(output).Matches(t => t.NotInlined);
        }

        [Test]
        public void ShouldInlineConstantProperty()
        {
            // arrange
            Expression<Func<Target, int>> input = t => t.Inlined;

            this.resolver.ForMember<Func<Target, int>>(x => x.Inlined).Returns(x => 5);

            // act
            var output = this.inliner.Inline(input);

            // assert
            Asserts.That(output).Matches(t => 5);
        }

        [Test]
        public void ShouldInlinePropertyWhichReferencesThis()
        {
            // arrange
            Expression<Func<Target, int>> input = t => t.Inlined;

            this.resolver.ForMember<Func<Target, int>>(x => x.Inlined).Returns(x => x.NotInlined);

            // act
            var output = this.inliner.Inline(input);

            // assert
            Asserts.That(output).Matches(t => t.NotInlined);
        }

        [Test]
        public void ShouldInlineMethodWithoutParameters()
        {
            // arrange
            Expression<Func<Target, int>> input = t => t.Method();

            this.resolver.ForMember<Func<Target, int>>(x => x.Method()).Returns(x => x.NotInlined);

            // act
            var output = this.inliner.Inline(input);

            // assert
            Asserts.That(output).Matches(t => t.NotInlined);
        }

        [Test]
        public void ShouldInlineMethodWithParameters()
        {
            // arrange
            Expression<Func<Target, int>> input = t => t.Method(t.NotInlined);

            this.resolver.ForMember<Func<Target, int, int>>(x => x.Method(x.NotInlined)).Returns((x, i) => 5 + i);

            // act
            var output = this.inliner.Inline(input);

            // assert
            Asserts.That(output).Matches(t => 5 + t.NotInlined);
        }

        [Test]
        public void ShouldInlineMethodWithParametersAndThisReference()
        {
            // arrange
            Expression<Func<Target, int>> input = t => t.Method(t.NotInlined);

            this.resolver.ForMember<Func<Target, int, int>>(x => x.Method(x.NotInlined)).Returns((x, i) => x.NotInlined + 5 + i);

            // act
            var output = this.inliner.Inline(input);

            // assert
            Asserts.That(output).Matches(t => t.NotInlined + 5 + t.NotInlined);
        }
    }
}
