﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Inline
{
    public class Target
    {
        public int NotInlined { get; set; }

        public int Inlined { get; set; }

        public int Method(int x)
        {
            return 0;
        }

        public int Method()
        {
            return 0;
        }
    }
}
