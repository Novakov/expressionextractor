﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    public static class Helper
    {
        public static Expression<TExpression> GetExpression<TExpression>(Expression<TExpression> member)
        {
            if (member.Body.NodeType == ExpressionType.Call)
            {
                var call = (MethodCallExpression)member.Body;

                return (Expression<TExpression>)GetValue(call.Method.DeclaringType.Namespace + ".Expr." + call.Method.DeclaringType.Name + ", " + call.Method.DeclaringType.Assembly.FullName, call.Method.Name);
            }

            if (member.Body.NodeType == ExpressionType.MemberAccess)
            {
                var access = (MemberExpression)member.Body;

                var fieldName = access.Member.Name;

                if (access.Member is PropertyInfo)
                {
                    fieldName = (access.Member as PropertyInfo).GetMethod.Name;
                }

                return (Expression<TExpression>)GetValue(access.Member.DeclaringType.Namespace + ".Expr." + access.Member.DeclaringType.Name + ", " + access.Member.DeclaringType.Assembly.FullName, fieldName);
            }

            return null;
        }

        private static object GetValue(string typeName, string field)
        {
            var t = Type.GetType(typeName);

            if (t == null)
            {
                throw new InvalidOperationException("Type " + typeName + " not found");
            }

            return t.GetField(field).GetValue(null);
        }
    }
}
