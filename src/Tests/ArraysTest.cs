﻿using NUnit.Framework;
using SampleDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class ArraysTest : ExpressionTestBase
    {
        [Test]
        public void NonZeroSizeArrayTest()
        {
            ExpressionEqual<Func<Arrays, int[]>>(a => a.NonZeroSizeArray(), a => new int[5]);
        }

        [Test]
        public void ZeroSizeArrayTest()
        {
            var expected = Expression.Lambda<Func<Arrays, int[]>>
                (
                    Expression.NewArrayBounds(typeof(int), Expression.Constant(0)),
                    Expression.Parameter(typeof(Arrays))
                );

            ExpressionEqual<Func<Arrays, int[]>>(a => a.ZeroSizeArray(), expected);
        }

        [Test]
        public void ArrayOfPrimitiveTypesTest()
        {
            ExpressionEqual<Func<Arrays, int[]>>(a => a.ArrayOfPrimitiveTypes(), a => new int[] { 1, 2, 3, 4, 5 });
        }

        [Test]
        public void ArrayOfReferenceTypesTest()
        {
            ExpressionEqual<Func<Arrays, object[]>>(a => a.ArrayOfReferenceTypes(), s => new[] { new object(), new object() });
        }

        [Test]
        public void ArrayOfReferenceTypesWithInitializer()
        {
            ExpressionEqual<Func<Arrays, object[]>>(a => a.ArrayOfReferenceTypesWithInitializer(), s => new object[] { new object(), new CtorCalls(66) { X = 2 } });
        }

        [Test]
        public void ArrayOfReferenceTypesWithNestedInitializer()
        {
            Expression<Func<Arrays, object[]>> expected = a => new object[] 
            { 
                new object(), 
                new CtorCalls(66) 
                { 
                    X = 2, 
                    Sub = new CtorCalls(77) 
                    { 
                        X = 88 
                    } 
                } 
            };

            ExpressionEqual<Func<Arrays, object[]>>(a => a.ArrayOfReferenceTypesWithNestedInitializer(), expected);
        }
    }
}
