﻿using NUnit.Framework;
using SampleDomain;
using System;

namespace Tests
{
    [TestFixture]
    public class StepsTest : ExpressionTestBase
    {
        [Test]
        public void ConstantTest()
        {
            ExpressionEqual<Func<Steps, int>>(s => s.Constant(), s => 5);
        }

        [Test]
        public void PropertyReferenceTest()
        {
            ExpressionEqual<Func<Steps, int>>(s => s.PropertyReference(), s => s.Age);
        }

        [Test]
        public void IndirectPropertyReferenceTest()
        {
            ExpressionEqual<Func<Steps, int>>(s => s.IndirectPropertyReference(), s => s.Name.Length);
        }

        [Test]
        public void BinaryOperatorTest()
        {
            ExpressionEqual<Func<Steps, int>>(s => s.BinaryOperator(), s => s.Age + 5);
        }

        [Test]
        public void UnaryOperatorTest()
        {
            ExpressionEqual<Func<Steps, bool>>(s => s.UnaryOperator(), s => !s.Citizen);
        }

        [Test]
        public void UnaryMinusOperatorTest()
        {
            ExpressionEqual<Func<Steps, int>>(s => s.MinusAge(), s => -s.Age);
        }

        [Test]
        public void IncrementOperatorTest()
        {
            ExpressionEqual<Func<Steps, int>>(s => s.AgePlusOne(), s => s.Age + 1);
        }

        [Test]
        public void FieldReferenceTest()
        {
            ExpressionEqual<Func<Steps, int>>(s => s.FieldReference(), s => s.field);
        }

        [Test]
        public void StaticPropertyReferenceTest()
        {
            ExpressionEqual<Func<Steps, int>>(s => s.StaticPropReference(), s => Steps.StaticProp);
        }

        [Test]
        public void StaticFieldReferenceTest()
        {
            ExpressionEqual<Func<Steps, int>>(x => x.StaticFieldReference(), x => Steps.staticField);
        }

        [Test]
        public void StaticMethodReferenceTest()
        {
            ExpressionEqual<Func<Steps, int>>(x => x.StaticMethodReference(), x => Steps.StaticMethod(5));
        }

        [Test]
        public void CoalesceTest()
        {
            ExpressionEqual<Func<Steps, int>>(s => s.Coalesce(), s => (s.nullbleInt ?? 7) + 2);
        }
    }
}
