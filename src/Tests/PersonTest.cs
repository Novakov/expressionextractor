﻿using NUnit.Framework;
using SampleDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class PersonTest
    {
        [Test]
        public void EnumEqualityTest()
        {
            var expr = Helper.GetExpression<Func<Person, bool>>(p => p.NeverMarried);

            Asserts.Equal(p => p.MaritalStatus == MaritalStatus.Single, expr);
        }

        [Test]
        public void EnumInEqualityTest()
        {
            var expr = Helper.GetExpression<Func<Person, bool>>(p => p.IsFree);

            Asserts.Equal(p => p.MaritalStatus != MaritalStatus.Married, expr);
        }

        [Test]
        public void MethodCallTest()
        {
            var expr = Helper.GetExpression<Func<Person, DateTime, bool>>((p, d) => p.IsAdult(d));

            Asserts.Equal((p, d) => p.Age(d) > 18, expr);
        }

        [Test]
        public void AgeTest()
        {
            var expr = Helper.GetExpression<Func<Person, DateTime, double>>((p, d) => p.Age(d));

            Asserts.Equal((p, d) => (d - p.BirthAt).TotalDays / 365.0, expr);
        }
    }
}
