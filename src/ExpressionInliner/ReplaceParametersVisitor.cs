﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace ExpressionInliner
{
    internal class ReplaceParametersVisitor : ExpressionVisitor
    {
        private Dictionary<ParameterExpression, Expression> parameterMap;

        public ReplaceParametersVisitor(Dictionary<ParameterExpression, Expression> parameterMap)
        {
            this.parameterMap = parameterMap;
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            return this.parameterMap[node];
        }
    }
}
