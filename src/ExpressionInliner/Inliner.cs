﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionInliner
{
    public class Inliner
    {
        private IMemberResolver memberResolver;

        public Inliner(IMemberResolver memberResolver)
        {
            this.memberResolver = memberResolver;
        }

        public TExpression Inline<TExpression>(TExpression expression)
            where TExpression : Expression
        {
            return (TExpression)new InlineVisitor(this.memberResolver).Visit(expression);
        }
    }
}
