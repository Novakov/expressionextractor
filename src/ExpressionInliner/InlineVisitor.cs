﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace ExpressionInliner
{
    internal class InlineVisitor : ExpressionVisitor
    {
        private IMemberResolver memberResolver;

        public InlineVisitor(IMemberResolver memberResolver)
        {
            this.memberResolver = memberResolver;
        }

        private Expression ResolveAndVisit(MemberInfo member, Expression node)
        {
            var replacement = this.memberResolver.Resolve(member);

            if (replacement != null)
            {
                return this.Visit(replacement.Body);
            }
            else
            {
                return node;
            }
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            var replacement = this.memberResolver.Resolve(node.Member);

            if (replacement != null)
            {
                return this.Visit(replacement.Body);
            }
            else
            {
                return node;
            }
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            var replacement = this.memberResolver.Resolve(node.Method);

            if (replacement != null)
            {
                var arguments = new List<Expression>(node.Arguments);

                if (!node.Method.IsStatic)
                {
                    arguments.Insert(0, node.Object);
                }

                var parameterMap = arguments
                    .Zip(replacement.Parameters, (a, p) => new { Argument = a, Parameter = p })
                    .ToDictionary(x => x.Parameter, x => x.Argument);

                return this.Visit(new ReplaceParametersVisitor(parameterMap).Visit(replacement.Body));
            }
            else
            {
                return node;
            }
        }
    }
}
